package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Branch;
import model.Client;
import model.ComparatorBranchByDistance;
import model.Instance;
import model.Position;
import model.Solution;
import model.Solver;

public class BranchTest {

	@Test
	public void branchDistanceTest() {
		Solver solver = new Solver(instance(), new ComparatorBranchByDistance());
		Solution solution = solver.solve();		
		assertTrue(solution.branchs.get(0).totalDistance == 20.11977681474567);
	}

	@Test
	public void branchCantClientTest() {
		Solver solver = new Solver(instance2(), new ComparatorBranchByDistance());
		Solution solution = solver.solve();
		assertTrue(solution.branchs.get(0).cantClients == 2);
	}

	private Instance instance() {
		Instance ret = new Instance(2);
		ret.addClient(new Client("Client1", new Position(-34.51396523410709, -58.73299598693848)));
		ret.addBranch(new Branch("Suc1", new Position(-34.511436831813484, -58.73110771179199)));

		return ret;
	}

	private Instance instance2() {
		Instance ret = new Instance(2);
		ret.addClient(new Client("Client1", new Position(-34.51396523410709, -58.73299598693848)));
		ret.addClient(new Client("Client2", new Position(-34.51396523410709, -58.73299598693848)));
		ret.addBranch(new Branch("Suc1", new Position(-34.511436831813484, -58.73110771179199)));

		return ret;
	}

}
