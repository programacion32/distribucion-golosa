package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Branch;
import model.Client;
import model.ComparatorBranchByDistance;
import model.Instance;
import model.Position;
import model.Solution;
import model.Solver;

public class ClientTest {

	@Test
	public void clientTestClosestBranch() {
		Instance inst = instance();
		Solver solver = new Solver(inst, new ComparatorBranchByDistance());
		Solution solution = solver.solve();	
		
		assertTrue(solution.clients.get(0).closestBranch.getName().equals("Suc1"));
	}
	
	@Test
	public void clientTestNotClosestBranch() {
		Instance inst = instance();
		Solver solver = new Solver(inst, new ComparatorBranchByDistance());
		Solution solution = solver.solve();
		
		assertFalse(solution.clients.get(0).closestBranch.getName().equals("Suc3"));
	}
	

	
	private Instance instance() {
		Instance ret = new Instance(2);
		ret.addClient(new Client("Client1", new Position(-34.51396523410709, -58.73299598693848)));
		// sucursal mas proximo a client1
		ret.addBranch(new Branch("Suc1", new Position(-34.511436831813484, -58.73110771179199)));
				
		// sucursal segundo mas proximo a client1
		ret.addBranch(new Branch("Suc2", new Position(-34.512815969848056, -58.72673034667969)));
		
		// sucursal mas lejana a client1
		ret.addBranch(new Branch("Suc3", new Position(-35.50775901879784, -58.72771739959717)));

		return ret;
	}

}
