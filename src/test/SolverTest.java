package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Branch;
import model.Client;
import model.ComparatorBranchByDistance;
import model.Instance;
import model.Position;
import model.Solution;
import model.Solver;

public class SolverTest {

	@Test
	public void solveByDistanceTest() {
		Solver solver = new Solver(instance(), new ComparatorBranchByDistance());
		Solution solution = solver.solve();
		assertTrue(solution.branchs.size() == 2 && solution.branchs.get(0).getName().equals("Suc1")
				&& solution.branchs.get(1).getName().equals("Suc2"));
	}
	
	@Test
	public void solveByDistanceBranchSizeTest() {
		Solver solver = new Solver(instance(), new ComparatorBranchByDistance());
		Solution solution = solver.solve();
		assertTrue(solution.branchs.size() == 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setMaxTotalBranchNegative() {
		Instance inst = instance();
		inst.setMaxTotalBranch(-1);
	}
	
	@Test
	public void solveByDistanceMaxTotalBranchZeroTest() {
		Instance inst = instance();
		inst.setMaxTotalBranch(0);
		Solver solver = new Solver(inst, new ComparatorBranchByDistance());
		Solution solution = solver.solve();
		assertTrue(solution.branchs.size() == 0);
	}

	private Instance instance() {
		Instance ret = new Instance(2);
		ret.addClient(new Client("Client1", new Position(-34.51396523410709, -58.73299598693848)));
		ret.addClient(new Client("Client2", new Position(-34.51460174287586, -58.72591495513916)));
		ret.addClient(new Client("Client3", new Position(-34.51030521432701, -58.72859716415405)));
		ret.addClient(new Client("Client4", new Position(-34.51610459150799, -58.729519844055176)));
		ret.addClient(new Client("Client5", new Position(-34.511224654705295, -58.72299671173096)));
		ret.addClient(new Client("Client6", new Position(-34.50977476334036, -58.733510971069336)));
		ret.addClient(new Client("Client7", new Position(-34.512373940912255, -58.73293161392212)));

		ret.addBranch(new Branch("Suc1", new Position(-34.511436831813484, -58.73110771179199)));
		ret.addBranch(new Branch("Suc2", new Position(-34.512815969848056, -58.72673034667969)));
		ret.addBranch(new Branch("Suc3", new Position(-34.51016376106072, -58.72473478317261)));
		ret.addBranch(new Branch("Suc4", new Position(-34.516652682498425, -58.72816801071167)));
		ret.addBranch(new Branch("Suc5", new Position(-34.50775901879784, -58.72771739959717)));

		return ret;
	}

}
