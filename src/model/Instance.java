package model;

import java.util.ArrayList;

public class Instance {

	private int maxTotalBranch;
	private ArrayList<Client> clients;
	private ArrayList<Branch> branchs;

	public Instance() {
		this.clients = new ArrayList<Client>();
		this.branchs = new ArrayList<Branch>();
	}

	public Instance(int maxTotalBranch) {
		this.clients = new ArrayList<Client>();
		this.branchs = new ArrayList<Branch>();
		this.checkPositive(maxTotalBranch);
		this.maxTotalBranch = maxTotalBranch;
	}

	public void addClient(Client client) {
		this.clients.add(client);
	}

	public void addBranch(Branch branch) {
		this.branchs.add(branch);
	}

	public int getMaxTotalBranch() {
		return this.maxTotalBranch;
	}

	public void setMaxTotalBranch(int maxTotalBranch) {
		checkPositive(maxTotalBranch);
		this.maxTotalBranch = maxTotalBranch;
	}

	private void checkPositive(int val) {
		if (val < 0)
			throw new IllegalArgumentException("Se cargo valor negativo :" + val);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Client> getClients() {
		return (ArrayList<Client>) this.clients.clone();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Branch> getBranch() {
		return (ArrayList<Branch>) this.branchs.clone();
	}

}
