package model;

import java.util.ArrayList;

public class Solution {	
	
	public ArrayList<Client> clients;
	public ArrayList<Branch> branchs;

	public Solution() {		
		this.branchs = new ArrayList<Branch>();
	}
	
	//Calcula la distancia total de cada sucursal entre todos los clientes
	public void calcDistanceOfEachBranch() {
		for(Branch b : branchs) {
			b.calcTotalDistance(clients);				
		}
	}

}
