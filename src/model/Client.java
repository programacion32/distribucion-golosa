package model;

import java.util.ArrayList;

public class Client extends Entity {

	public Branch closestBranch;

	public Client(String name, Position position) {
		super(name, position);
	}

	public Entity SearchClosestEntity(ArrayList<Branch> otherEntities) {
		double distance = 0;
		for (Branch ent : otherEntities) {
			double tempDistance = super.calcDistanceTo(ent);
			if (distance == 0 || distance > tempDistance) {
				distance = tempDistance;
				this.closestBranch = ent;
			}
		}
		return closestBranch;
	}
}
