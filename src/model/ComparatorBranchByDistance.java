package model;

import java.util.Comparator;

public class ComparatorBranchByDistance implements Comparator<Branch> {
	@Override
	public int compare(Branch uno, Branch otro) {
		return (int) (uno.getTotalDistance() - otro.getTotalDistance());
	}
}
