package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Solver {

	private Instance instance;
	private Comparator<Branch> comparator;
	private Solution retSolution;

	public Solver(Instance instance, Comparator<Branch> comparator) {
		this.instance = instance;
		this.comparator = comparator;
	}

	public Solution solve() {

		retSolution = new Solution();

		retSolution.clients = this.instance.getClients();
		retSolution.branchs = this.instance.getBranch();

		retSolution.calcDistanceOfEachBranch(); // calculo la distancia total de cada sucursal

		retSolution.branchs = sortFilterBranch(); // filtro por el maximo de sucursales permitidos

		// Por cada cliente se fija cual sucursal del conujunto le queda mas cerca
		ArrayList<Branch> filterList = sortFilterBranch();
		for (Client c : retSolution.clients) {
			c.SearchClosestEntity(filterList);			
		}
		for (Branch br : retSolution.branchs) {
			br.setCantClients(retSolution.clients);
		}

		return retSolution;
	}

	public ArrayList<Branch> sortFilterBranch() {
		ArrayList<Branch> retFilterBranch = retSolution.branchs;

		Collections.sort(retFilterBranch, this.comparator);

		ArrayList<Branch> filtered = new ArrayList<Branch>();
		if(this.instance.getMaxTotalBranch()>retFilterBranch.size()) {
			this.instance.setMaxTotalBranch(retFilterBranch.size());
		}
		for (int i = 0; i < this.instance.getMaxTotalBranch(); i++)
			filtered.add(retFilterBranch.get(i));

		return filtered;
	}
}
