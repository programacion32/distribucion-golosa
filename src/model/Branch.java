package model;

import java.util.ArrayList;

public class Branch extends Entity {

	public double totalDistance;
	public int cantClients;

	public Branch(String name, Position position) {
		super(name, position);
	}

	public void calcTotalDistance(ArrayList<Client> clients) {
		this.totalDistance = 0;
		for (Client client : clients) {
			totalDistance += super.calcDistanceTo(client);
		}
	}

	public double getTotalDistance() {
		return this.totalDistance;
	}

	public void setCantClients(ArrayList<Client> clients) {
		this.cantClients = 0;
		for (Client c : clients) {
			if (c.closestBranch.getName().equals(super.getName())) {
				cantClients++;
			}
		}
	}

}
