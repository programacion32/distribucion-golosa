package model;

public abstract class Entity {

	private String name;
	private Position position;

	public Entity(String name, Position position) {
		this.name = name;
		this.position = position;
	}

	protected double calcDistanceTo(Entity other) {
		double r = 6378;

		double lat1 = this.position.latitude;
		double lon1 = this.position.longitude;
		double lat2 = other.getPosition().latitude;
		double lon2 = other.getPosition().longitude;

		double deltaLat = lat2 - lat1;
		double deltaLon = lon2 - lon1;
		double calc1 = Math.pow(Math.sin(deltaLat / 2), 2)
				+ Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
		double calc2 = 2 * Math.atan2(Math.sqrt(calc1), Math.sqrt(1 - calc1));

		return r * calc2;
	}

	public String getName() {
		return name;
	}

	public Position getPosition() {
		return this.position;
	}

}
