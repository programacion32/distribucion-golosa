package service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FileManager {
	public String fullPath;

	public FileManager() {
		this.fullPath = "";
	}

	public FileManager(String fullpath) {
		this.fullPath = fullpath;
	}

	public FileManager(String fileName, String path) {

		this.fullPath = path + fileName;
	}

	public void generateGson(Object obj) {
		try {
			checkFullPath();
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(obj);			
			FileWriter fw = new FileWriter(this.fullPath);
			fw.write(json);
			fw.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public Object readGson(Object objTypeOff) {
		Gson gson = new Gson();
		Object obj = objTypeOff;
		try {
			checkFullPath();
			BufferedReader bf = new BufferedReader(new FileReader(this.fullPath));
			obj = gson.fromJson(bf, obj.getClass());
		} catch (Exception e) {
			System.out.println(e);
		}
		return obj;
	}

	private void checkFullPath() throws Exception {
		if (fullPath.isBlank())
			throw new Exception("El path contienen datos vacios");
	}

}
