package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

import java.awt.Color;

import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import model.Branch;
import model.Client;
import model.ComparatorBranchByDistance;
import model.Instance;
import model.Position;
import model.Solution;
import model.Solver;
import service.FileManager;

import javax.swing.JTextPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainView {

	private Instance instance;
	private String path;
	private String fileName;
	private Solver solver;
	private Solution solution;

	private JFrame frame;

	private JTextField textEntity;
	private JTextField textFieldEntityName;
	private JTextField textFieldEntityLat;

	private JLabel lblOpenFolder;
	private JLabel lblEntityType;
	private JLabel lblEntityUbicacion;
	private JLabel lblEntityName;
	private JLabel lblEntity;

	private JButton btnLoadEntitys;
	private JButton btnEntityAdd;

	private JMapViewer jmapv;

	private JPanel panelMap;
	private JPanel panelLoad;
	private JPanel panelEntityAdd;
	private JPanel panelStats;

	private JFileChooser fc;

	private JSpinner spinnerEntityType;

	private JTextPane textPaneStats;
	private JTextField textFieldEntityLong;
	private JButton btnEntityConect;

	/**
	 * Create the application.
	 */
	public MainView() {
		this.instance = new Instance();
		initialize();
	}

	public void addClient(String name, Position p) {
		Client cl = new Client(name, p);
		this.instance.addClient(cl);
		this.addMapMarkerClientView(name, p);
	}

	public void addBranch(String name, Position p) {
		Branch br = new Branch(name, p);
		this.instance.addBranch(br);
		this.addMapMarkerBranchView(name, p);
	}

	private void addMapMarkerBranchView(String name, Position p) {
		Coordinate c = new Coordinate(p.latitude, p.longitude);
		MapMarker m = new MapMarkerDot(name, c);
		m.getStyle().setBackColor(Color.RED);
		m.getStyle().setColor(Color.BLACK);
		this.jmapv.addMapMarker(m);
	}

	private void addMapMarkerClientView(String name, Position p) {
		Coordinate c = new Coordinate(p.latitude, p.longitude);
		MapMarker m = new MapMarkerDot(name, c);
		this.jmapv.addMapMarker(m);
	}

	private void currentLocationDirectory() {
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		textEntity.setText(s + "\\src\\service\\precarga.json");
	}

	public void saveGsonFile() {
		System.out.println(textEntity.getText());
		FileManager fm = new FileManager(textEntity.getText());
		fm.generateGson(this.instance);
	}

	public void loadGsonFile() {
		System.out.println(textEntity.getText());
		FileManager fm = new FileManager(textEntity.getText());
		this.instance = (Instance) fm.readGson(this.instance);
		reloadMapMarks(this.instance.getBranch(), this.instance.getClients());
	}

	private void reloadMapMarks(ArrayList<Branch> branches, ArrayList<Client> clients) {
		this.jmapv.removeAllMapMarkers();
		for (Branch b : branches) {
			this.addMapMarkerBranchView(b.getName(), b.getPosition());
		}
		for (Client c : clients) {
			this.addMapMarkerClientView(c.getName(), c.getPosition());
		}
	}

	private void conectClientsWithClosestBranchs(ArrayList<Client> clients) {
		this.jmapv.removeAllMapPolygons();
		for (Client cl : clients) {
			ArrayList<Coordinate> route = new ArrayList<Coordinate>();
			Coordinate coordClient = new Coordinate(cl.getPosition().latitude, cl.getPosition().longitude);
			Coordinate coordBranch = new Coordinate(cl.closestBranch.getPosition().latitude,
					cl.closestBranch.getPosition().longitude);

			route.add(coordBranch);
			route.add(coordClient);
			route.add(coordBranch);
			this.jmapv.addMapPolygon(new MapPolygonImpl(route));
		}

	}

	private void estadistics(ArrayList<Branch> branchs) {
		StringBuilder data = new StringBuilder();
		String tab = "\t| ";
		data.append("Sucursal"+tab+"Costo\t"+tab+"Clientes\n");
		for(Branch br : branchs)
			data.append(br.getName()+tab+br.totalDistance+tab+br.cantClients+"\n");
			
		textPaneStats.setText(data.toString());
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize() {
		frameInitialize();

		jMapViewerPanel();

		entityPreload();

		entityNew();

		stats();

		options();
	}

	private void frameInitialize() {
		frame = new JFrame();
		frame.setTitle("Distribucion Golosa");
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(110, 110, 1090, 644);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		frame.setResizable(false);
	}

	private void jMapViewerPanel() {
		panelMap = new JPanel();
		panelMap.setBounds(352, 11, 712, 583);
		frame.getContentPane().add(panelMap);
		jmapv = new JMapViewer();
		jmapv.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String lat = jmapv.getPosition(e.getPoint()).getLat() + "";
				String lon = jmapv.getPosition(e.getPoint()).getLon() + "";
				textFieldEntityLat.setText(lat);
				textFieldEntityLong.setText(lon);
			}
		});
		jmapv.setBounds(10, 11, 692, 561);
		jmapv.setZoomControlsVisible(false);
		jmapv.setDisplayPosition(new Coordinate(-34.511861184409106, -58.72838258743286), 16);
		panelMap.setLayout(null);
		panelMap.add(jmapv);
	}

	private void entityPreload() {
		panelLoad = new JPanel();
		panelLoad.setBounds(10, 11, 332, 109);
		frame.getContentPane().add(panelLoad);
		panelLoad.setLayout(null);

		textEntity = new JTextField();
		textEntity.setHorizontalAlignment(SwingConstants.LEFT);

		currentLocationDirectory();

		textEntity.setToolTipText("");
		textEntity.setBounds(10, 41, 264, 20);
		panelLoad.add(textEntity);
		textEntity.setColumns(10);

		JLabel lblTitlePreload = new JLabel("Precarga");
		lblTitlePreload.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitlePreload.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitlePreload.setBounds(10, 11, 312, 20);
		panelLoad.add(lblTitlePreload);

		lblOpenFolder = new JLabel("");
		lblOpenFolder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				fileChooserPopUp();
			}
		});

		lblOpenFolder.setHorizontalAlignment(SwingConstants.CENTER);
		lblOpenFolder.setIcon(new ImageIcon(MainView.class.getResource("/images/iconfolder.png")));
		lblOpenFolder.setBounds(284, 41, 38, 23);
		panelLoad.add(lblOpenFolder);

		JButton btnSaveEntitys = new JButton("Save");
		btnSaveEntitys.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveGsonFile();
			}
		});
		btnSaveEntitys.setBounds(68, 72, 98, 23);
		panelLoad.add(btnSaveEntitys);

		btnLoadEntitys = new JButton("Load");
		btnLoadEntitys.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadGsonFile();
			}
		});
		btnLoadEntitys.setBounds(176, 72, 98, 23);
		panelLoad.add(btnLoadEntitys);
	}

	private void fileChooserPopUp() {
		fc = new JFileChooser();

		fc.showOpenDialog(panelMap);

		this.path = fc.getCurrentDirectory().toString();
		this.fileName = fc.getSelectedFile().getName();

		textEntity.setText(this.path + "\\" + this.fileName);
	}

	private void entityNew() {
		panelEntityAdd = new JPanel();
		panelEntityAdd.setBounds(10, 131, 332, 157);
		frame.getContentPane().add(panelEntityAdd);
		panelEntityAdd.setLayout(null);

		lblEntityType = new JLabel("Tipo:");
		lblEntityType.setBounds(54, 91, 73, 20);
		panelEntityAdd.add(lblEntityType);

		lblEntityUbicacion = new JLabel("Lat | Lon:");
		lblEntityUbicacion.setBounds(54, 66, 73, 20);
		panelEntityAdd.add(lblEntityUbicacion);

		lblEntityName = new JLabel("Nombre:");
		lblEntityName.setBounds(54, 42, 73, 20);
		panelEntityAdd.add(lblEntityName);

		lblEntity = new JLabel("Entidad");
		lblEntity.setHorizontalAlignment(SwingConstants.CENTER);
		lblEntity.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEntity.setBounds(10, 11, 312, 20);
		panelEntityAdd.add(lblEntity);

		textFieldEntityName = new JTextField();
		textFieldEntityName.setColumns(10);
		textFieldEntityName.setBounds(124, 42, 157, 20);
		panelEntityAdd.add(textFieldEntityName);

		textFieldEntityLat = new JTextField();
		textFieldEntityLat.setColumns(10);
		textFieldEntityLat.setBounds(124, 66, 73, 20);
		panelEntityAdd.add(textFieldEntityLat);

		textFieldEntityLong = new JTextField();
		textFieldEntityLong.setColumns(10);
		textFieldEntityLong.setBounds(207, 66, 73, 20);
		panelEntityAdd.add(textFieldEntityLong);

		spinnerEntityType = new JSpinner();
		spinnerEntityType.setModel(new SpinnerListModel(model.EntityType.values()));
		spinnerEntityType.setBounds(124, 91, 157, 20);
		panelEntityAdd.add(spinnerEntityType);

		btnEntityAdd = new JButton("Agregar");
		btnEntityAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String name = textFieldEntityName.getText();
				Double lat = Double.parseDouble(textFieldEntityLat.getText());
				Double lon = Double.parseDouble(textFieldEntityLong.getText());

				switch (spinnerEntityType.getValue().toString()) {
				case "CLIENT":
					addClient(name, new Position(lat, lon));
					break;

				case "BRANCH":
					addBranch(name, new Position(lat, lon));
					break;
				}

			}
		});
		btnEntityAdd.setBounds(124, 122, 89, 23);
		panelEntityAdd.add(btnEntityAdd);

	}

	private void stats() {
		panelStats = new JPanel();
		panelStats.setBounds(10, 299, 332, 295);
		frame.getContentPane().add(panelStats);
		panelStats.setLayout(null);

		textPaneStats = new JTextPane();
		textPaneStats.setBounds(10, 42, 312, 242);
		panelStats.add(textPaneStats);

		JLabel lblEstadisticas = new JLabel("Estadisticas");
		lblEstadisticas.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstadisticas.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEstadisticas.setBounds(10, 11, 312, 20);
		panelStats.add(lblEstadisticas);
	}

	private void options() {

		JSpinner spinnerBranchMaxFilter = new JSpinner();
		spinnerBranchMaxFilter.setModel(new SpinnerNumberModel(2, 0, null, 1));
		spinnerBranchMaxFilter.setBounds(532, 498, 51, 20);
		jmapv.add(spinnerBranchMaxFilter);

		JButton btnNewButton = new JButton("Filtrar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				instance.setMaxTotalBranch((int) spinnerBranchMaxFilter.getValue());
				solver = new Solver(instance, new ComparatorBranchByDistance());
				solution = solver.solve();
				reloadMapMarks(solution.branchs, solution.clients);
				estadistics(solution.branchs);
			}
		});

		btnNewButton.setBounds(593, 497, 89, 23);
		jmapv.add(btnNewButton);

		btnEntityConect = new JButton("Conectar Puntos");
		btnEntityConect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				conectClientsWithClosestBranchs(solution.clients);
			}
		});
		btnEntityConect.setBounds(532, 527, 150, 23);
		jmapv.add(btnEntityConect);
	}
}
